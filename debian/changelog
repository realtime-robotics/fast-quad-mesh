fast-quad-mesh (0.1.0-2) unstable; urgency=medium

  [ Will Floyd-Jones ]
  * Added marching cubes and edge decimation

  [ Neil Roza ]
  * fixing all package.xml versions: 0.1.1
  * FAST-QUADRIC-MESH-SIMPLIFICATION: fix it and its dependent, rtr-geometry
  * put warning flags into every CMakeLists.txt
  * version: 0.1.1 -> 0.1.2
  * cmake 2.8.7->3.0.2, GNUInstallDirs, rtr-api udev rules
  * bump version 1.0.0
  * bump version 1.1.0
  * vendor librealsense2 rapid-480

  [ Brian Jin ]
  * Trajectory Test Infrastructure (#723)
    * initial commmit; missing collision robot
    * compiling hell
    * print statements; array3d bug
    * added raster voxelizer option; untested
    * minor print statment change
    * tests for jsoncpp
    * really bad implementation of jsoncpp; compiles but floating point exception
    * addressed floating point bug
    * refactor
    * additonal refactor
    * remove comments from cmakelist
    * refactor; changes to array3d
    * added set function; rebased to master
    * refactor + save before 'big' change
    * minor refactor
    * switched to vector of voxels
    * diff voxels stored as tuple
    * small refactor
    * made functions constant
    * can run without ros
    * fix typo
    * json -> struct
    * fixed find closest voxel function
    * removed unused var
    * removed jsoncpp from cmakelists
    * spline type to struct
    * bug fixes to voxelizerrobot
    * iostream
    * inital commit of class; does not compile
    * compiles
    * works
    * cleanup
    * custom hash table
    * getters for hash table
    * initial commit of unit test
    * reorganized tests
    * marker array unit test
    * formatting
    * moved visualizer to rtr-occupancy package
    * fixed data save bug; remove jsoncpp from cmake
    * simplified struc
    * initial commit of struct
    * copied some functions
    * comment change
    * reorganized structs
    * added comments
    * fixed cmake spacing
    * removed debug names
    * new ros package; updated package xml
    * updated rtr-occupancy package xml
    * intgers set with u
    * changed version number
    * added comments
    * refactor test
    * added csv reader function; refactoring
    * refactor
    * moved func from hpp to cpp
    * switched to rostest

  [ Will Floyd-Jones ]
  * Mesh simplify cleanup (#1109)
    * Cleaned up the mesh simplifier
    * Removing uncalled code
    * Updated comment at top of Hpp
    * That was broken somehow, but it's fixed now
    * Got rid of a nampspace
    * Added actual multithread test
    * Addressed PR review changes

  [ Neil Roza ]
  * bump version 1.2.0
  * bump version 1.3.0

  [ Bryce Willey ]
  * C++14 Migration (#2249)
    ROS Melodic supports C++14, which means that it is possible to update and upgrade our compiler to use that. We get a few template goodies (automatic function return type deduction, generic lambdas), binary literals, make_unique, and decimal separators. And less pain when we want to jump to C++17.
    https://realtimerobotics.atlassian.net/browse/RAPID-3501
    * Change CXX_STANDARDs to C++ 14
    * Replace unique_ptr with make_unique
    * Add digit separators to long (>= 6 digit) numbers
    * Can't `make_unique` if the constructor is private
  * cmakelint git hook addition (#2927)
    There is now a new git hook that enforces a standard set of linter rules on CMakeLists.txts, the key ones being a line length of 100, indention amount of 2 spaces, and enforcing lower casing for all CMake commands. All of the rules are at https://github.com/richq/cmake-lint#usage.
    The rest of the PR fixes those existing issues in the codebase, so other don't have to fix things.
    ## Tests
    During development, made sure that the git hook would fire for wrong CMakeLists, and otherwise still print ALL GOOD

  [ realtime-neil ]
  * RAPID-5935: third-party licenses (#3257)
    ====
    Add and/or install licenses for various third-party projects.
    * edit `src/third-party/fast-quadric-mesh-simplification/CMakeLists.txt`:
      install `LICENSE.md` to `${CATKIN_PACKAGE_SHARE_DESTINATION}`
    * add `src/third-party/fast-quadric-mesh-simplification/LICENSE.md`
    * edit `src/third-party/isg-sphere-tree-construction/CMakeLists.txt`: install
      `COPYRIGHT.txt` to `${CATKIN_PACKAGE_SHARE_DESTINATION}`
    * edit `src/third-party/matplotlib-cpp/CMakeLists.txt`: install `LICENSE` and
      `LICENSE.matplotlib` to `${CATKIN_PACKAGE_SHARE_DESTINATION}`
    * edit `src/third-party/miniball/CMakeLists.txt`: install `README.md` to
    `${CATKIN_PACKAGE_SHARE_DESTINATION}`
    * edit `src/third-party/miniball/package.xml`: add authors, license, website
      url, and repository url
    * edit `src/third-party/nlohmann-json/CMakeLists.txt`: install `LICENSE.MIT` to
      `${CATKIN_PACKAGE_SHARE_DESTINATION}`
    * edit `src/third-party/realsense2/CMakeLists.txt`: install `LICENSE` to
      `${CATKIN_PACKAGE_SHARE_DESTINATION}`
    * add `src/third-party/realsense2/LICENSE`
    * edit `src/third-party/rtr-msgpack/CMakeLists.txt`: install `LICENSE_1_0.txt`
    to `${CATKIN_PACKAGE_SHARE_DESTINATION}`
    * edit `src/third-party/rtr-spdlog/CMakeLists.txt`: install `LICENSE`,
      `tests/catch.license`, and `include/rtr-spdlog/fmt/bundled/LICENSE.rst` to
      `${CATKIN_PACKAGE_SHARE_DESTINATION}`
    * edit `src/third-party/tbb2/CMakeLists.txt`: install `LICENSE` to
    `${CATKIN_PACKAGE_SHARE_DESTINATION}`
    * add `src/third-party/tbb2/LICENSE`
    * edit `src/third-party/v-hacd/CMakeLists.txt`: install `LICENSE` to
      `${CATKIN_PACKAGE_SHARE_DESTINATION}`

  [ Neil Roza ]
  * bump version 1.4.0
  * bump version 1.4.1
  * bump version 2.0.0

  [ Muthu Arivoli ]
  * Migrate to ROS 2 (#3888)
    * migrate rtr_cmake_defs
    * add serial package
    * migrate third party packages
    * export interfaces over libraries
    * migrate rtr_utils
    * migrate rtr_math
    * migrate rtr_msgs
    * migrate rtr_geometry
    * migrate rtr_edt, rtr_occupancy, rtr_system, rtr_voxelize
    * migrate some more packages
    * add third-party industrial_msgs and simple_message
    * add trac_ik_lib and fcl_catkin
    * migrate many more packages
    * migrate rtr_ros_utils
    * fix file paths
    * Migrate most of rtr_control_ros
    Updates cmake files and package.xml for ament and colcon. Makes constant msg and srv fields all uppercase.
    Rewrites the RobotManager, RosController, and Trajectory classes to use rclcpp, for ROS 2. Updates most tests to use rclcpp and ROS 2 syntax.
    Still need to migrate the RobotManager tests and the launch files to ROS 2. Also need to migrate some python scripts in the src folder, although those seem to be unused.
    * build shared libs
    Updates add library commands to build for shared libraries, previously everything was built statically.
    * migrate rtr_toolkit
    Replace fcl_catkin with fcl due to build issues and update cmake and package files for the toolkit.
    * gutting debian/control
    * Build-Depends: +libnlopt-dev
    * Migrate most of rtr_optimization
    Migrates nearly all of optimization except for some of the launch files. Adds python helper functions to rtr_ros_utils.
    Also need to determine why the python code is not logging properly.
    * Migrate more non-ROS packages and optimization fixes
    Migrate rtr_og_apps, rtr_robot_maker, rtr_violet_tool, rtr_perc_api, rtr_perc_calibration, rtr_perc_calibration,
    rtr_perc_rapidsense, rtr_perc_sensors, rtr_perc_spatial, rtr_perc_ui, rtr_wb_app_layer. Updates their
    CMakeLists.txt and package.xml file for ament/ROS 2 and other minor changes for the ROS package path.
    Fixes optimization by changing the node names set in the code to be the same as in the launch file, since the
    launch files were not properly changing them.
    * Migrate rtr_world_builder and rtr_perc_sensors_ros and fix optimization
    Migrates rtr_world_builder by updating the optimization client to use ROS 2, and migrates all
    of the code in rtr_perc_sensors_ros.
    Updates optimization to use separate callback groups for clients and services due to deadlocks
    with the default execution model in ROS 2 when calling clients in the callback of a service.
    * Upgrade to Ubuntu 20.04 and migrate rtr_perc_spatial_cuda
    Updates code to build on 20.04. Main changes include updating boost error code conditions
    to not compare with nullptr, that was removed in the update Boost library (now v1.71). Also
    updates already migrated perception code to use OpenCV 4.
    Various logging calls that logged nlohmann json vars have been disabled due to  breaking
    the build, will need to figure out.
    * Migrate rtr_dsm
    * CMake updates
    Moves from deprecated ament_export_interfaces to ament_export_targets new in Foxy.
    Refactors find package ament_cmake_gtest into cmake function in the cmake_defs.
    Uses target based installs for the include directories, removing ament_export_include.
    * Migrate rtr_perc_rapidsense_ros
    Also adds a helper function to rtr_ros_utils for sync client calls.
    * Migrate rtr_spatial_perception
    Migrates all of rtr_spatial_perception except for the launch files.
    * ed debian/control: Build-Depends: rm python
    * ed debian/control: Build-Depends: rm ros-foxy-control-msgs
    * Revert "ed debian/control: Build-Depends: rm ros-foxy-control-msgs"
    This reverts commit dfe785462538d748fc2267277e0b9eb09a3f14b3.
    * top-level CMakeLists.txt for colcon
    * Revert "ed debian/control: Build-Depends: rm python"
    This reverts commit 749be205617b52ad976e23174b11bd807c794188.
    * no moar python2
    * Update top-level CMakelists.txt to use external_project_add
    * Update fcl build to use ament over pkg-config
    * Migrate rtr_rapidsense_gui
    * Migrate rtr_appliance
    Also makes some updates to rtr_ros_utils to pass nodes into certain classes.
    * Migrate rtr_app_commander and rtr_appliance_app and install fcl with ament
    For fcl, moves from using pkgconfig to ament for the top-level CMakelists.txt to work.
    * Add test deps to debian/control
    * Update shebangs to python3 for already migrated code
    * Add ament_package calls for violet packages when disabled
    * Remove deleted message include
    * Add pcl_conversions and tf2_ros to debian/control
    * ed debian/control: rm python
    * Fix versions for debian/control
    * Various updates and fixes
    Changes all package path getters to use share directory.
    Replaces rest of client calls to use helper function.
    Uninstall ROS signal handlers appropriately.
    Create AsyncExecutor helper to cleanly deal with spin thread destruction.
    Fixes parameters calls to use get_parameter.
    * Migrate rtr_appliance_ros
    RosApplianceBagger does not work currently, python code has been converted to Python 3.
    Also fixes some threading issues in optimization.
    * Update debian/control
    * Fix optimization build
    * update scripts/ci
    * ed scripts/ci: parallel-workers: nproc/4 -> nproc/2
    * Try setting base path in ci
    * Fix tests and build
    Updates packages with external interfaces (optimization, control_ros) to use different
    library names so that the build system and during runtime, we can find the .so libraries
    with the definitons.
    Comments one dsm tests that is failing due to unknown reasons.
    * ed debian/control: nodejs: 14 -> 12
    * Increase time limit for tests to be one hour
    * Set one hour test timeout for rtr_app_layer
    * Try dsm test on ci
    * ed debian/control: add more Build-Depends and sort
    * ed debian/control: Build-Depends: +ros-foxy-std-srvs, +ros-foxy-tf2-eigen
    * Migrate rtr_appliance_webapp
    * Fix and remove unused code from webapp
    * Migrate all launch files
    Only a couple have been tested such as optimization_main and wb.
    * Fix webapp
    * Update readme and update services for ros2
    * Fix scripts
    Updates python scripts for python3
    Updates config files to use install space, ros2 commands
    Updates bash scripts for colcon or deletes them
    * Add rosdaemon service
    * ExecStart _ros2_daemon
    * just invoke _ros2_daemon
    `_ros2_daemon` is a python3 executable script and sourcing the ROS `setup.sh`
    puts it into the `$PATH`
    * Remove setup.py from rtr_control_ros
    * Use char vector for msgpack schema messages in dsm
    Fixes the dsm observer unit test
    * Remove comments from dsm fix
    * Remove rtr_control_ros python tests and fix dsm test
    * Change dsm timeout to pass test
    * Try to speed up ci with cmake_build_type
    * Disable ProjectYaml test
    * Migrate rtr_control_ros tests
    * Fix SOVERSION in cmake files
    * Try to install into INSTALL_PREFIX for top level cmake
    * Try to fix deb build by localizing external project build
    * Fix variable rootfs
    * reformat externalproject_add CMAKE_ARGS
    * force BINARY_DIR to be PWD-relative
    * reverse PREFIX path components
    * Remove different architectures from ref-napi via postinstall script
    * add some helper functions
    * cmake_minimum_required: 3.5 -> 3.7
    * add comment about CMAKE_FLAGS
    * default CMAKE_INSTALL_PREFIX and others
    * no more sniffing ROS_DISTRO in rtr_cmake_defs luser_candy
    * get fake install and rootfs working
    * Fixes for package locations and shared libraries
    * Update debian/rules to manually invoke cmake and make for each package
    Also reverts updates to luser_candy.cmake, moving back to ROOTFS over FAKE_ROOTFS.
    * Override dh_prep to not remove install generated in build step
    * Try with buildsystem cmake
    * add debian/changelog
    * get sub-makes working on build and test
    * Recreate top-level cmakelists.txt and update debian/rules
    * ed debian/rules: export ROS_DISTRO
    * Remove remnants of catkin and don't install models+configs for optimization in deb build
    * Remove optimization data and disable optimization tests
    * Remove deleted message types from merge
    * Fix webapp bin file and update terminator config files
    * Create GetNextMessage subscription helper to make code threadsafe
    * Fix deadlocks in rapidsense and updates to executor creation
    * Disable LinkTest in rtr_core due to segfault
    Confirmed this is also failing in ros1, so disabling
    * debian/rules better vars; CMakeLists.txt MFLAGS
    * ed debian/rules: stop spitting env
    * ed CMakeLists.txt: better diagnostics verbiage
    * ed CMakeLists.txt: env override and default for MFLAGS
    * ed CMakeLists.txt: whoops
    * ed debian/rules: remove pedantic comments about $ROS_ROOT/setup.sh
    * ed CMakeLists.txt: use $(MAKE) commands to quash "warning: jobserver unavailable"
    * Remove pluginlib from cmakelists and disable some tests
    * Remove omp deps that no longer exist in focal
    * Remove custom robot_state_publisher package and other fixes
    * Use separate cb group for clients
    * Add ros-foxy-ros2cli to debian/control
    * Add ros-base to debian/control
    * RCUTILS_LOGGING_USE_STDOUT=1 some debian/rapidplan.*.service
    * Fix package names in systemd service files
    * Remove yaskawa_move_to_joint.py and change ABB plugin version to 1.4.0
    * ed debian/postinst: pycompile -> py3compile
    * ros logging is special
    * Maintain source permissions in deb
    * ed debian/control
    Build-Depends: add `ros-foxy-ros-base`, remove (recursive) dependencies of
    `ros-foxy-ros-base`.
    Depends: remove (recursive) dependencies of `ros-foxy-ros-base`, sort
    * Update AppCommanderRosHelpers.py
    * Update PythonApplianceCommander.py
    * Fix cppheaderparser package.xml
    * Fix optimization deb build and fix world builder developer shortcuts
    * Review fixes
    * Fix webapp deb build to generate ts definitions for rtr_msgs
    * Use separate cb group for rapidsense subs
    * ed debian/control: Depends: add libboost-date-time1.71.0
    * ed debian/control: Depends: add ros-foxy-vision-opencv
    * Remove rosdaemon service
    * Remove setting HOME env var with new release of ros-foxy-launch
    This can now use ROS_HOME to set the log directory.
    * Restrict ROS to use localhost only
    * pyclean -> py3clean to fix deb package uninstall
    * Remove last remnants of catkin
    * Migrate test-mpa
    * Migrate test-rapidplan-metal to colcon
    * Add argcomplete for autocomplete to python deps
    * Fix webapp joint states not updating
    Co-authored-by: Neil Roza <neil@rtr.ai>

  [ Neil Roza ]
  * bump version 2.0.1

  [ howieChiang ]
  * update debian/control file

  [ Neil Roza ]
  * update debian/control

  [ howieChiang ]
  * rename fast-quad-mesh to prepend 'lib'
  * update name of package with version number
  * upgrade format to native
  * remove ros related lines in CMakefile
  * update package name to `fast-quad-mesh`
  * remove installation path /usr/share
  * clean up CMakeLists.txt
  * edit debian/rules and update CMakeLists
  * install header files to CMAKE_INSTALL_FULL_INCLUDEDIR

  [ Neil Roza ]
  * add debian/gbp.conf

  [ howieChiang ]
  * 1. install entirity of inc directory 2. fix header path in src files

  [ Neil Roza ]
  * wrap-and-sort -abst

  [ howieChiang ]
  * add cmake as a build dependency
  * bump version name of libfast-quad-mesh

 -- howie <howie@rtr.ai>  Fri, 18 Mar 2022 12:02:28 -0400

fast-quad-mesh (0.1.0-1) unstable; urgency=medium

  * Initial release (Closes: #nnnn)  <nnnn is the bug number of your ITP>

 -- howie <howie@rtr.ai>  Tue, 15 Mar 2022 13:09:02 -0400
