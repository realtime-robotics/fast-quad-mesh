////////////////////////////////////////////////////////////////////////////////
// Mesh Simplification Library
// (C) by Sven Forstmann in 2014
// License : MIT
// http://opensource.org/licenses/MIT
// https://github.com/sp4cerat/Fast-Quadric-Mesh-Simplification
//
// Modified by willf-j October 2019 for clean up and improve encapsulation
////////////////////////////////////////////////////////////////////////////////

#ifndef SIMPLIFY_HPP
#define SIMPLIFY_HPP

#include <string.h>
#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>

namespace Simplify {

double min(double v1, double v2);

struct vec3f {
  vec3f(void) {}
  vec3f(const double X, const double Y, const double Z);

  vec3f operator+(const vec3f &a) const;
  vec3f operator+=(const vec3f &a) const;
  vec3f operator*(const double a) const;
  vec3f operator*(const vec3f a) const;
  vec3f operator=(const vec3f a);
  vec3f operator/(const vec3f a) const;
  vec3f operator-(const vec3f &a) const;
  vec3f operator/(const double a) const;
  double dot(const vec3f &a) const;
  vec3f cross(const vec3f &a, const vec3f &b);
  double length() const;
  vec3f normalize();

  double x, y, z;
};

vec3f barycentric(const vec3f &p, const vec3f &a, const vec3f &b,
                  const vec3f &c);
vec3f interpolate(const vec3f &p, const vec3f &a, const vec3f &b,
                  const vec3f &c, const vec3f attrs[3]);

////////////////////////////////////////////////////////////////////////////////
// Global Variables & Structures
////////////////////////////////////////////////////////////////////////////////

class SymetricMatrix {
 public:
  SymetricMatrix(double c = 0);
  SymetricMatrix(double m11, double m12, double m13, double m14, double m22,
                 double m23, double m24, double m33, double m34, double m44);

  // Make plane
  SymetricMatrix(double a, double b, double c, double d);

  double operator[](int c) const;

  // Determinant
  double det(int a11, int a12, int a13, int a21, int a22, int a23, int a31,
             int a32, int a33);

  const SymetricMatrix operator+(const SymetricMatrix &n) const;
  SymetricMatrix &operator+=(const SymetricMatrix &n);

  double m[10];
};

enum Attributes { NONE, NORMAL = 2, TEXCOORD = 4, COLOR = 8 };

struct Triangle {
  int v[3];
  double err[4];
  int deleted, dirty, attr;
  vec3f n;
  vec3f uvs[3];
  int material;
};

struct Vertex {
  vec3f p;
  int tstart, tcount;
  SymetricMatrix q;
  int border;
};

struct Ref {
  int tid, tvertex;
};


////////////////////////////////////////////////////////////////////////////////

struct Simplifier {
  // target_count  : target nr. of triangles
  // agressiveness : sharpness to increase the threshold.
  //                 5..8 are good numbers
  //                 more iterations yield higher quality
  void simplify_mesh(int target_count, double agressiveness = 7,
                     bool verbose = false);
  void simplify_mesh_lossless(bool verbose = false);

  bool flipped(vec3f p, int i0, int i1, Vertex &v0, Vertex &v1,
               std::vector<int> &deleted);

  void update_uvs(int i0, const Vertex &v, const vec3f &p,
                  std::vector<int> &deleted);

  // Update triangle connections and edge error after a edge is collapsed
  void update_triangles(int i0, Vertex &v, std::vector<int> &deleted,
                        int &deleted_triangles);

  // compact triangles, compute edge error and build reference list
  void update_mesh(int iteration);

  // Finally compact mesh before exiting
  void compact_mesh();

  // Error between vertex and Quadric
  double vertex_error(SymetricMatrix q, double x, double y, double z);

  // Error for one edge
  double calculate_error(int id_v1, int id_v2, vec3f &p_result);

  std::vector<Triangle> triangles;
  std::vector<Vertex> vertices;
  std::vector<Ref> refs;
  std::string mtllib;
  std::vector<std::string> materials;
};

} // namespace Simplify

#endif  // SIMPLIFY_HPP
